"""Functionality related to interfacing with the Twi-XL HTTP API."""
# nopycln: file
import twixl.__version__
from enum import Enum
from typing import Optional, Text, Union, no_type_check, List

from furl import furl
from pydantic import BaseModel
from requests import Session, Response


class QueryState(Enum):
    """
    API query state, identical to the state of Athena's QueryExecutionStatus. See:
    https://docs.aws.amazon.com/athena/latest/APIReference/API_QueryExecutionStatus.html
    """

    CANCELED = "CANCELED"
    FAILED = "FAILED"
    RUNNING = "RUNNING"
    QUEUED = "QUEUED"
    SUCCEEDED = "SUCCEEDED"


class QueryStatus(BaseModel):
    """API query status containing the state of the query, optional output location (a URL), and the amount of data
    scanned, in bytes."""

    state: QueryState
    location: List[Optional[str]]
    datascanned: int  # TODO: use a unit (Pint)?


class QueryId(BaseModel):
    id: str


class APISession(Session):
    def __init__(self, api_endpoint: str, api_key: str):
        super().__init__()
        self.api_endpoint = furl(api_endpoint)
        self.api_key = api_key
        self.headers.update({"x-api-key": api_key})
        self.headers.update({"x-client-version": twixl.__version__})

    @no_type_check
    def request(
        self, method: str, url: Union[bytes, str, Text], *args, **kwargs
    ) -> Response:
        return super().request(method, self.api_endpoint / url, *args, **kwargs)

    def request_safe(
        self, method: str, url: Union[bytes, str, Text], *args, **kwargs
    ) -> Response:
        response = self.request(method, url, *args, **kwargs)
        response.raise_for_status()
        return response
