import datetime


class QueryFailed(Exception):
    """The query failed."""

    def __init__(self, message: str = "query has failed"):
        super().__init__(message)


class QueryCanceled(Exception):
    """The query is canceled."""

    def __init__(self, message: str = "query was canceled"):
        super().__init__(message)


class QueryTimeout(Exception):
    """The query timed out."""

    def __init__(self, timeout: datetime.timedelta):
        super().__init__(
            f"query has timed out (timeout = {timeout.total_seconds()} seconds)"
        )
        self.timeout = timeout
