###################################
API Reference
###################################

.. module:: twixl.collections.twinl

This part of the documentation covers all the interfaces of twinl.

Main interface
-------------------

All of the twinls' functionality can be accessed by the following functions and classes.

.. autofunction:: search
.. autofunction:: word_frequency
.. autofunction:: tweet_metrics
.. autoclass:: Query
	:inherited-members:
.. autofunction:: API


Plotting functons
-------------------

This section of the documentation covers all the plotting functions.

.. module:: twixl.collections.twinl.plotting

.. autofunction:: plot_tweet_frequencies
.. autofunction:: plot_word_cloud
.. autofunction:: plot_circular_bars


Exceptions
-------------------

This section of the documentation covers all the exceptions.

.. module:: twixl.collections.twinl.exceptions

.. autoexception:: QueryFailed
.. autoexception:: QueryCanceled
.. autoexception:: QueryTimeout

Lower-Level Classes
-------------------

.. module:: twixl.collections.twinl
    :noindex:

.. autoclass:: SearchResults
    :inherited-members:
.. autoclass:: WordFrequencyResults
    :inherited-members:
.. autoclass:: TweetMetrics
    :inherited-members:
