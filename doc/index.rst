.. twi-xl-python documentation master file, created by
   sphinx-quickstart on Wed Mar  2 16:27:21 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to twi-xl-python's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   architecture_overview.rst
   api.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
