# Twi-XL Python package
The Twi-XL Python package is a Python package that works with the Twi-XL API.

## Installation
Install from git with pip against one of the available [tags](https://gitlab.com/twi-xl-surf-nl/twi-xl-python/-/tags):

```bash
pip install git+https://gitlab.com/twi-xl-surf-nl/twi-xl-python.git@1.1.0
```

## Issues

When you have a twi-xl enhancement request or identified a software problem or bug, create a gitlab issue. Gitlab issues can be created in the Issues tab by selecting the option "New issue". When you create an Issue, make sure to enter the following fields:
- Title: Give your issue a nice title
- Type: select Issue
- Description: Give a detailed description of the issue you want to address.
  In case of a bug, describe the actual behavior, the behavior you expected and attach any screenshots/code/logs that is related to the issue.
  In case of an enhancement/feature request, describe in great detail the feature/enhancement you would like to have and why (i.e. what's the research question behind this request).
- Labels: Select on of the following labels
    - bug: when you want to report a software problem
    - enhancement: when you want to request an extension of the twi-xl functionality
    - documentation: when your issue is related to the twi-xl documentation

## Development

### Prerequisites

#### Pre-Commit

Please install [pre-commit](https://pre-commit.com/) and install the pre-commit hooks with:

```bash
pre-commit install
```

This will automatically invoke pre-commit for each commit, and run the [pre-commit hooks](./.pre-commit-config.yaml)

#### Poetry

Please install [poetry](https://python-poetry.org/docs/). Poetry is a tool for dependency management and packaging in Python.

To initialize this project, run the following command:

```bash
cd twi-xl-python
poetry init
```

To add required packages to the twi-xl package run the following command:

```bash
# add pendulum package to twi-xl package
poetry add pendulum
```

In order to get the latest versions of all the dependencies run the following command:

```bash
poetry update
```

### Branching & merge requests

There are two developer roles in this project: developers and maintainers. Maintainers have GitLab Maintainer rights, developers Developer rights. Developers are not allowed to push directly to the master branch of the gitops repository. Instead, developers create (feature) branches and merge into the master branch by means of merge requests. Merge requests can only be merged by maintainers.

This guide assumes the perspective of a developer. If you are a maintainer, you can push directly to master.

First, branch off from master:

```bash
git checkout -b feature/new-plot-option
```

Implement the changes that you want to make. Commit the changed file(s) and then push it to the feature branch you created. Go to your latest commit on GitLab, and it will show a 'Create merge request' button. Click this button to create a merge request, and assign it to one or more of the project's maintainers for approval.

After a maintainer has approved and merged your changes, they will be present in the master branch.
